Source: maven-antrun-plugin
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Ludovic Claude <ludovic.claude@laposte.net>,
 Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 ant (>= 1.8.2),
 debhelper-compat (= 13),
 default-jdk,
 libeclipse-sisu-maven-plugin-java,
 libmaven-parent-java,
 libmaven-invoker-plugin-java (>= 1.3),
 libmaven-plugin-tools-java,
 libmaven3-core-java,
 libmodello-maven-plugin-java,
 libplexus-utils-java,
 maven-debian-helper
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/java-team/maven-antrun-plugin.git
Vcs-Browser: https://salsa.debian.org/java-team/maven-antrun-plugin
Homepage: http://maven.apache.org/plugins/maven-antrun-plugin/

Package: libmaven-antrun-plugin-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Recommends: ${maven:OptionalDepends}
Description: Maven AntRun Plugin
 Maven is a software project management and comprehension tool. Based on the
 concept of a project object model (POM), Maven can manage a project's build,
 reporting and documentation from a central piece of information.
 .
 Maven's primary goal is to allow a developer to comprehend the complete
 state of a development effort in the shortest period of time. In order to
 attain this goal there are several areas of concern that Maven attempts
 to deal with:
 .
    * Making the build process easy
    * Providing a uniform build system
    * Providing quality project information
    * Providing guidelines for best practices development
    * Allowing transparent migration to new features
 .
 The Maven Antrun Plugin runs Ant scripts embedded in the POM
